<?php

namespace judahnator\DiscordHttpWrapper;

use Dotenv\Dotenv;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

/**
 * A wrapper around the guzzle/guzzle class,
 * providing an easier interface between this
 * application and HTTP endpoints.
 *
 * For more information on Guzzle visit its
 * Github repository at:
 * https://github.com/guzzle/guzzle
 *
 * @package judahnator\DiscordHttpWrapper
 */
final class Guzzle extends Client
{
    private $RateLimit = [];

    /**
     * GuzzleInterface constructor.
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {

        // If the env is not already setup, load it
        if (getenv('BOT_TOKEN') === false) {

            // ensure the environment is setup
            $dotenv = new Dotenv(__DIR__, '../.env');
            $dotenv->load();
        }

        // Bootstrap some default variables
        $config['base_uri'] = 'https://discordapp.com/api/v6/';
        $config['headers']['Authorization'] = 'Bot '.getenv('BOT_TOKEN');
        $config['headers']['User-Agent'] = 'judahnator/discord-http-wrapper (https://gitlab.com/judahnator/discord-http-wrapper)';

        parent::__construct($config);
    }

    /**
     * Returns this singleton instance.
     *
     * @return Guzzle
     */
    public static function getInstance()
    {
        static $Instance = null;

        if (is_null($Instance)) {
            $Instance = new self();
        }

        return $Instance;
    }

    /**
     * Return a json decoded response from a URL
     *
     * @param string $uri
     * @param array $options
     * @return mixed
     */
    public static function getJson($uri, array $options = []) {
        return json_decode(static::getInstance()->get($uri,$options)->getBody());
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array  $options
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function request($method, $uri = '', array $options = [])
    {

        // Find the route we are querying, eg "channel" or "guild" or "users"
        $route = explode('/', trim(str_replace('api/v6/', '', $uri), '/'))[0];

        if (

            // Checks to see if a prior request to this route has been made
            // If this check fails, this is the first request to this route
            // and we do not need to worry about breaching the rate limit
            array_key_exists($route, $this->RateLimit) &&
            array_key_exists('X-RateLimit-Reset', $this->RateLimit[$route]) &&
            array_key_exists('X-RateLimit-Remaining', $this->RateLimit[$route]) &&

            // Here we check to see if we at at the rate limit
            time() < $this->RateLimit[$route]['X-RateLimit-Reset'] &&
            $this->RateLimit[$route]['X-RateLimit-Remaining'] === 0

        ) {

            // If we have met the rate limit, simply "pause" until we can make another request
            sleep($this->RateLimit[$route]['X-RateLimit-Reset'] - time());
        }

        try {

            // Get the response from the parent classes function
            $response = parent::request($method, $uri, $options);

            // Before returning the response, be sure to update the ratelimit data
            // with the info provided by the Discord servers.
            // Also a good idea to check to see if the headers exist before attempting
            // to access them
            if ($response->hasHeader('X-RateLimit-Reset')) {
                $this->RateLimit[$route]['X-RateLimit-Reset'] = (int) $response->getHeader('X-RateLimit-Reset')[0];
            }

            if ($response->hasHeader('X-RateLimit-Remaining')) {
                $this->RateLimit[$route]['X-RateLimit-Remaining'] = (int) $response->getHeader('X-RateLimit-Remaining')[0];
            }

            if ($response->hasHeader('X-RateLimit-Limit')) {
                $this->RateLimit[$route]['X-RateLimit-Limit'] = (int) $response->getHeader('X-RateLimit-Limit')[0];
            }

            // Return the response to the client
            return $response;
        } catch (ClientException $exception) {

            /*
             * If there is only one instance of this bot running
             * then this will never be executed. This is only in
             * place for if multiple concurrent instances of the
             * bot are running, in which case the header info may
             * not be accurate.
             *
             * If we hit the rate limit it will be safe to assume
             * that there is more than one instance of this script
             * running concurrently. In this case we should add a
             * comfortable buffer to the retry_after field.
             */
            if ($exception->getResponse()->getStatusCode() === 429) {

                // Get the response body
                $ResponseBody = json_decode($exception->getResponse()->getBody());

                // Sleep for the number of seconds specified plus random padding
                usleep($ResponseBody->retry_after + rand(100, 750));

                // Try the request again
                return $this->request($method, $uri, $options);
            }

            // If the exception was unknown, throw it back
            throw $exception;
        }
    }
}

<?php

namespace judahnator\DiscordHttpWrapper;

use GuzzleHttp\Exception\ClientException;
use judahnator\DiscordHttpWrapper\Exceptions\MessageNotFoundException;

/**
 * Class Message.
 *
 * @property array attachments
 * @property Author author
 * @property int author_id
 * @property Channel channel
 * @property int channel_id
 * @property string content
 * @property string|null edited_timestamp
 * @property int id
 * @property bool mention_everyone
 * @property bool pinned
 * @property string timestamp
 * @property bool tts
 */
class Message implements \Serializable
{
    private $attributes = [];

    // A cache for this messages author
    private $Author = null;

    private $Channel = null;

    public function __construct(\stdClass $attributes)
    {
        // Setup the basic message attributes
        $this->attributes['author_id'] = $attributes->author->id;
        $this->attributes['channel_id'] = $attributes->channel_id;
        $this->attributes['content'] = $attributes->content;
        $this->attributes['edited_timestamp'] = $attributes->edited_timestamp;
        $this->attributes['id'] = $attributes->id;
        $this->attributes['mention_everyone'] = $attributes->mention_everyone;
        $this->attributes['pinned'] = $attributes->pinned;
        $this->attributes['timestamp'] = $attributes->timestamp;
        $this->attributes['tts'] = $attributes->tts;

        // Set the attachments
        $this->attributes['attachments'] = [];
        foreach ($attributes->attachments as $attachment) {
            $this->attributes['attachments'][] = new Attachment($attachment);
        }
    }

    public function __get($name)
    {
        switch ($name) {

            case 'author':
                return $this->getAuthor();

            case 'channel':
                return $this->getChannel();

            case 'guild':
                return $this->channel->guild;

            case array_key_exists($name, $this->attributes):
                return $this->attributes[$name];

        }
    }

    public static function find(int $ChannelId, int $MessageId)
    {
        try {

            // Create a new message instance and return it
            return new self(Guzzle::getJson("channels/$ChannelId/messages/$MessageId"));

        } catch (ClientException $clientException) {

            // If the message could not be found throw an exception
            if ($clientException->getResponse()->getStatusCode() === 404) {
                throw new MessageNotFoundException('The requested message could not be found');
            }

            throw $clientException;
        }
    }

    /**
     * Fetches the author for the message.
     *
     * @return Author
     */
    private function getAuthor()
    {
        if (is_null($this->Author)) {
            $this->Author = Author::find($this->attributes['author_id']);
        }

        return $this->Author;
    }

    private function getChannel()
    {

        // If the channel is not cached then cache it
        if (is_null($this->Channel)) {
            $this->Channel = Channel::find($this->channel_id);
        }

        // Return the cached channel
        return $this->Channel;
    }

    /**
     * Deletes the current message.
     *
     * @return bool
     */
    public function delete()
    {
        Guzzle::getInstance()
            ->delete('channels/' . $this->channel->id . '/messages/' . $this->id);

        return true;
    }

    /**
     * Replies to the author of the current message.
     *
     * @param string $Content
     *
     * @return Message
     */
    public function reply($Content)
    {
        return static::send((int)$this->channel_id, '<@' . $this->author_id . '>	' . $Content);
    }

    /**
     * Sends a message in the designated channel.
     *
     * @param int|Channel $Channel
     * @param string      $Content
     *
     * @return Message
     */
    public static function send($Channel, $Content)
    {
        if (is_int($Channel)) {
            $channel_id = $Channel;
        } elseif (is_object($Channel) && get_class($Channel) === Channel::class) {
            $channel_id = $Channel->id;
        } else {
            throw new \InvalidArgumentException('The channel provided must be either an integer of type Channel');
        }

        $PostResponse = Guzzle::getInstance()
            ->post("channels/$channel_id/messages", [
                'json' => [
                    'content' => $Content,
                ],
            ]);

        return new self(json_decode($PostResponse->getBody()));
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize($this->attributes);
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        $this->attributes = unserialize($serialized);
    }
}

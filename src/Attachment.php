<?php

namespace judahnator\DiscordHttpWrapper;

/**
 * Class Attachment.
 */
class Attachment
{
    public $filename;
    public $height;
    public $id;
    public $proxy_url;
    public $size;
    public $url;
    public $width;

    public function __construct(\stdClass $attachmentData)
    {
        foreach (['filename', 'height', 'id', 'proxy_url', 'size', 'url', 'width'] as $variable) {
            $this->$variable = $attachmentData->$variable;
        }
    }
}

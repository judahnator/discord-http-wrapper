<?php

namespace judahnator\DiscordHttpWrapper;

use GuzzleHttp\Exception\ClientException;
use judahnator\DiscordHttpWrapper\Exceptions\AuthorNotFoundException;

/**
 * Class Author.
 *
 * @property string|null avatar
 * @property bool|null bot
 * @property int discriminator
 * @property string|null email
 * @property int id
 * @property bool|null mfa_enabled
 * @property string username
 * @property bool|null verified
 */
class Author implements \Serializable
{
    /**
     * @var array
     */
    private $attributes = [];

    /**
     * Author constructor.
     *
     * @param int $AuthorID
     *
     * @throws AuthorNotFoundException
     */
    public function __construct(int $AuthorID)
    {
        try {

            // Fetch the user data
            $UserResponse = Guzzle::getInstance()
                ->get('users/'.$AuthorID)->getBody();

            // Decode the response and set the attributes
            $this->attributes = json_decode($UserResponse, true);

            // Make sure integers are actually an int
            $this->attributes['discriminator'] = (int) $this->attributes['discriminator'];
            $this->attributes['id'] = (int) $this->attributes['id'];
        } catch (ClientException $exception) {
            if ($exception->getResponse()->getStatusCode() === 404) {
                throw new AuthorNotFoundException('The given author could not be found');
            }
        }
    }

    /**
     * @param $name
     *
     * @return mixed|null
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->attributes)) {
            return $this->attributes[$name];
        }
    }

    /**
     * Static loader for this class.
     *
     * @param int $AuthorID
     *
     * @return Author
     */
    public static function find(int $AuthorID)
    {
        return new self($AuthorID);
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize($this->attributes);
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        $this->attributes = unserialize($serialized);
    }
}

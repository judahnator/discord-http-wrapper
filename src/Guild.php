<?php

namespace judahnator\DiscordHttpWrapper;

use GuzzleHttp\Exception\ClientException;
use judahnator\DiscordHttpWrapper\Exceptions\UnauthorizedException;

// TODO implement roles

/**
 * Class Guild.
 *
 * @property int id
 * @property array channels
 * @property array members
 * @property string name
 * @property int owner_id
 * @property string region
 */
class Guild implements \Serializable
{
    // The attributes of this guild
    private $attributes = [];

    // The cached channels for this guild
    private $Channels = [];

    // The cached members for this guild
    private $Members = [];

    public function __construct(int $GuildID)
    {

        try {

            // Get the info for this guild
            $GuildRequest = Guzzle::getJson('guilds/'.$GuildID);

            // Set the guild attributes
            $this->attributes['id'] = $GuildRequest->id;
            $this->attributes['name'] = $GuildRequest->name;
            $this->attributes['owner_id'] = $GuildRequest->owner_id;
            $this->attributes['region'] = $GuildRequest->region;

        } catch (ClientException $exception) {

            // If this use does not have read access to that guild
            if ($exception->getResponse()->getStatusCode() === 403) {
                throw new UnauthorizedException('You are not authorized to view this guild');
            }
        }

    }

    public function __get($name)
    {
        switch ($name) {

            case array_key_exists($name, $this->attributes):
                // If the attribute is readable return it
                return $this->attributes[$name];

            case 'channels':
                // Return the channels
                return $this->getChannels();

            case 'members':
                // return the members
                return $this->getMembers();

        }

        // otherwise just return null
    }

    /**
     * Static class loader.
     *
     * @param int $GuildID
     *
     * @return Guild
     */
    public static function find(int $GuildID)
    {
        return new self($GuildID);
    }

    /**
     * Returns an array of this guilds channels.
     *
     * @param bool $ForceUpdate
     *
     * @return array
     */
    public function getChannels(bool $ForceUpdate = false)
    {
        if (empty($this->Channels) || $ForceUpdate) {

            // Clear out the channel cache
            $this->Channels = [];

            // Fetch the channel data from the API
            $GuildChannels = Guzzle::getJson('guilds/'.$this->id.'/channels');

            // For each of the channels returned, push them onto the cached channels
            foreach ($GuildChannels as $channel) {
                $this->Channels[] = new Channel($channel->id);
            }
        }

        // Return the channels
        return $this->Channels;
    }

    /**
     * Returns an array of Author objects belonging to this guild.
     * Caution is advised when using on larger guilds.
     *
     * @param bool $ForceUpdate
     * @return array
     */
    public function getMembers(bool $ForceUpdate = false) {

        if (empty($this->Members) || $ForceUpdate) {

            $MembersResponse = Guzzle::getJson('guilds/'.$this->id.'/members');

            foreach ($MembersResponse as $MemberResponse) {
                $this->Members[] = Author::find($MemberResponse->user->id);
            }

        }

        return $this->Members;

    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize([
            'attributes' => $this->attributes,
            'channels' => $this->Channels,
            'members' => $this->Members
        ]);
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        $thisData = unserialize($serialized);
        $this->attributes = $thisData['attributes'];
        $this->Channels = $thisData['channels'];
        $this->Members = $thisData['members'];
    }
}

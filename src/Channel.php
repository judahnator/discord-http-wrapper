<?php

namespace judahnator\DiscordHttpWrapper;

use GuzzleHttp\Exception\ClientException;
use judahnator\DiscordHttpWrapper\Exceptions\ChannelNotFoundException;

/**
 * Class Channel.
 *
 * @property Guild guild
 * @property int guild_id
 * @property int id
 * @property bool is_private
 * @property int|null last_message_id
 * @property array messages
 * @property string name
 * @property string topic
 * @property string type
 */
class Channel implements \Serializable
{
    private $attributes = [];

    private $Guild = null;

    public function __construct(int $ChannelID)
    {

        try {

            $ChannelData = Guzzle::getJson('channels/'.$ChannelID);

            // Loop over the channel data and set this classes attributes
            foreach ($ChannelData as $element => $value) {
                $this->attributes[$element] = $value;
            }

        }catch (ClientException $exception) {

            if ($exception->getResponse()->getStatusCode() === 404) {
                throw new ChannelNotFoundException('The requested channel could not be found');
            }

            throw $exception;

        }

    }

    /**
     * @param $name
     *
     * @return mixed|void
     */
    public function __get($name)
    {

        // If the element is accessible then return it
        if (array_key_exists($name, $this->attributes)) {
            return $this->attributes[$name];
        }

        // If the user is asking for the messages return them
        if ($name === 'messages') {
            return $this->getMessages();
        }

        // If the user is asking for this channels guild, return it
        if ($name === 'guild') {
            return $this->getGuild();
        }
    }

    /**
     * Provided an array of Message::class objects OR message IDs, will bulk delete them.
     *
     * If more than 99 messages are provided it will "chunk" the array into smaller pieces
     * and bulk-delete those message blocks.
     *
     * If less than 2 messages are provided we cannot use the bulk-delete endpoint and must
     * delete the items individually.
     *
     * @param array $Messages
     *
     * @return bool
     */
    public function bulkDeleteMessages(array $Messages)
    {

        // An array of the items we will be removing
        $MessagesToDelete = [];

        // Loop through messages to sanatize the input
        foreach ($Messages as $message) {
            if (is_int($message)) {

                // If the input is an integer add it to the items to delete
                $MessagesToDelete[] = $message;
            } elseif (is_object($message) && get_class($message) === Message::class) {

                // If the input is of type Message::class then add its ID to the list
                $MessagesToDelete[] = $message->id;
            } else {

                // If we don't know what type it is, throw an exception
                throw new \InvalidArgumentException('I cannot delete this');
            }
        }

        if (count($MessagesToDelete) > 99) {

            // If there are too many items to delete in one pass, chunk by 99 and run this function again
            foreach (array_chunk($MessagesToDelete, 99) as $MessageBlock) {
                self::bulkDeleteMessages($MessageBlock);
            }
        } elseif (count($MessagesToDelete) <= 2) {

            // If there are too few items to delete we must remove them individually
            foreach ($MessagesToDelete as $MessageID) {
                Message::find($this->id, $MessageID)->delete();
            }
        } else {

            // Otherwise bulk delete these messages
            Guzzle::getInstance()
                ->post('channels/'.$this->id.'/messages/bulk-delete', [
                    'json' => [
                        'messages' => $MessagesToDelete,
                    ],
                ]);
        }

        return true;
    }

    /**
     * Static class loader.
     *
     * @param int $ChannelID
     *
     * @return Channel
     */
    public static function find(int $ChannelID)
    {
        return new self($ChannelID);
    }

    private function getGuild()
    {

        // If the guild is not cached then cache it
        if (is_null($this->Guild)) {
            $this->Guild = Guild::find($this->guild_id);
        }

        // Return the cached guild
        return $this->Guild;
    }

    /**
     * Returns an array of messages sent in this channel.
     *
     * You may pass in an array of options that the discord API supports.
     *
     * @see https://discordapp.com/developers/docs/resources/channel#get-channel-messages
     *
     * @return array
     */
    public function getMessages(array $options = [])
    {
        $MessagesData = Guzzle::getJson('channels/'.$this->id.'/messages', ['query' => $options]);

        $Messages = [];

        foreach ($MessagesData as $MessageData) {
            $Messages[] = new Message($MessageData);
        }

        return $Messages;
    }

    /**
     * Sends a message directly to the channel.
     *
     * @param string $content
     *
     * @return Message
     */
    public function sendMessage(string $content)
    {
        return Message::send($this, $content);
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize($this->attributes);
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        $this->attributes = unserialize($serialized);
    }
}

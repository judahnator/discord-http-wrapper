<?php

namespace judahnator\DiscordHttpWrapper;

/**
 * Class Bot.
 *
 * @property mixed avatar
 * @property bool bot
 * @property int discriminator
 * @property string|null email
 * @property array guilds
 * @property int id
 * @property bool mfa_enabled
 * @property string username
 * @property bool verified
 */
final class Bot
{
    // Cached guild data
    private $Guilds;

    // Info about this bot
    private $info = [];

    private function __construct()
    {
        // Set the basic info for this bot
        $MyInfo = Guzzle::getJson('users/@me');
        foreach ($MyInfo as $item => $value) {
            $this->info[$item] = $value;
        }
    }

    public function __get($name)
    {
        switch ($name) {

            case array_key_exists($name, $this->info):
                return $this->info[$name];

            case 'guilds':
                return $this->getGuilds();

        }
    }

    /**
     * Returns an array of the guilds this bot is in.
     *
     * @param bool $ForceUpdate
     *
     * @return array
     */
    public function getGuilds(bool $ForceUpdate = false)
    {

        // If no guilds are cached
        if (empty($this->Guilds) || $ForceUpdate) {

            // Clear out the cached guilds
            $this->Guilds = [];

            // Get the guilds we have joined
            $guilds = Guzzle::getJson('users/@me/guilds');

            // Fetch and add all the guilds
            foreach ($guilds as $guild) {
                $this->Guilds[] = new Guild($guild->id);
            }
        }

        // Return the guilds
        return $this->Guilds;
    }

    /**
     * Returns the singleton Bot instance.
     *
     * @return Bot
     */
    public static function Instance()
    {
        static $instance = null;

        if ($instance === null) {
            $instance = new self();
        }

        return $instance;
    }
}

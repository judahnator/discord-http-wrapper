Changelog
=========

v0.5
----
 * Updating all calls to the Discord API to use the v6 
 version, as below that will be deprecated in October. 
 See [the docs](https://discordapp.com/developers/docs/reference)
 for more information
 * Only incrementing to v0.5 (instead of 0.4.2) because
 this is a breaking change discord-side.

v0.4.1
------
 * Fixing bug where you could not serialize the Author,
 Channel, Guild, or Message classes.
 * Internal optimization of the Guzzle Wrapper.

v0.4.0
------
 * Adding a `getJson()` helper to the GuzzleInterface as
 a shortcut to just get the JSON body of a GET response.
 * Adding the ability to list the members in a guild.

v0.3.0
------
 * Adding the ability to delete messages both in bulk and
 individually.
 * Continuing to write tests to ensure functionality is
 as expected.

v0.1.0/v0.2.0
-------------
This were pre-alpha tags. They are not recommended for use.
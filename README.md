The Discord HTTP Wrapper
========================
[![Build Status](https://gitlab.com/judahnator/discord-http-wrapper/badges/master/pipeline.svg)](https://gitlab.com/judahnator/discord-http-wrapper/commits/master)

This library serves as a clean and simple interface between your bot 
and Discords HTTP REST API.

Because this library is under active development things might break, 
don't be afraid to open issues though. I will get to them as time permits.


Installation Instructions
-------------------------
This package can be installed via composer.
```
composer require judahnator/discord-http-wrapper
```
This library relies on the  [vlucas/phpdotenv](https://github.com/vlucas/phpdotenv) 
library to hold its configuration values. The most important of which 
being the token for your bot. If your project also utilizes this library
simply include `BOT_TOKEN=your-bots-token` in your .env file. Otherwise 
set that value in the `/vendor/judahnator/discord-http-wrapper/.env` file.
This file does not exist by default, but a `.env.example` template
is located in that same directory.

If you do not have a token yet, you can get one from the 
[Discord-developers site](https://discordapp.com/developers/applications/me).
Your token must also be authorized to make RESTful API requests.


Basic Usage
-----------
`judahnator\DiscordHttpWrapper\Bot::class` - A singleton class that
provides general info about the bot. For example:
```php
$Bot = judahnator\DiscordHttpWrapper\Bot::Instance();
$Bot->id; // 123456
$Bot->username; // TestBot
$Bot->guilds; // An array of guilds the bot has access to
```

`judahnator\DiscordHttpWrapper\Guild::class` - A class to interface with guilds.
```php
$Guild = judahnator\DiscordHttpWrapper\Guild::find(1234567890);
$Guild->id; // 1234567890
$Guild->name; // TestGuild
$Guild->channels; // An array of channels belonging to this guild
```

`judahnator\DiscordHttpWrapper\Channel::class` - A class to interface with channels.
```php
$Channel = judahantor\DiscordHttpWrapper\Channel::find(1234567890);
$Channel->id; // 1234567890
$Channel->name; // TestChannel
$Channel->guild; // The guild the channel belongs to
$Channel->messages; // The 50 most recent messages in this channel
$Channel->sendMessage("Foo bar baz"); // Sends message, returns the message object
```

`judahnator\DiscordHttpWrapper\Message::class` - Class to interface with messages
```php
$Message = judahnator\DiscordHttpWrapper\Message::find($ChannelID,$MessageID);
$Message->id; // Message ID
$Message->content; // The content of the message
$Message->author; // The author object
$Message->reply("Foo bar baz"); // Replies to the author of the current message

judahnator\DiscordHttpWrapper\Message::send($ChannelID,"Foo bar baz"); // Sends a message to a channel
```

`judahnator\DiscordHttpWrapper\Author::class` - Class to interface with authors
```php
$Author = judahnator\DiscordHttpWrapper\Author::find(1234567890);
$Author->id; // 1234567890
$Author->username; // TestAuthor
```

General Notes
-------------
As the name implies, this library interfaces with Discords HTTP API. This 
means that even on a low-latency connection, this library will be much slower
than other libraries that interface with Discord via websockets.

In the same line of thought, this library also respects the rate-limit headers
supplied by Discord. At the time of writing the limit was about 1.5 requests 
per second. This means in more popular servers, iterating through messages 
will be slow. If you need speed, I recommend using
[TeamReflexs' DiscordPHP](https://github.com/teamreflex/discordphp) library.

Contributing
------------
I welcome issues, forks, pull requests, whole nine yards. Just please for
the love of all that is good in the world write clean code and leave lots
of comments.
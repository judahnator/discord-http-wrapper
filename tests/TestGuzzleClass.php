<?php

namespace judahnator\DiscordHttpWrapper\Tests;

use judahnator\DiscordHttpWrapper\Guzzle;
use PHPUnit\Framework\TestCase;

class TestGuzzleClass extends TestCase
{
    public function setUp()
    {
        // Make sure the composer autoloader has been included
        require_once dirname(__DIR__).'/vendor/autoload.php';
        parent::setUp();
    }

    public function testBasicUsage()
    {

        // Query the API
        $ApiResponse = Guzzle::getJson('users/@me');

        // Make sure some expected info is present
        $this->assertTrue(is_string($ApiResponse->username), 'The username field was not a string');
        $this->assertTrue(is_numeric($ApiResponse->id), 'The id field was not numeric');
    }

    public function testRateLimiting()
    {
        for ($i = 0; $i < 15; $i++) {
            Guzzle::getJson('users/@me');
        }

        // If nothing unexpected happened it works
        // *crosses fingers*
        $this->assertTrue(true);
    }

    public function testGetJsonFunction() {
        $TestResponse = Guzzle::getJson('users/@me');
        $this->assertObjectHasAttribute('id',$TestResponse);
        $this->assertObjectHasAttribute('username',$TestResponse);
    }
}

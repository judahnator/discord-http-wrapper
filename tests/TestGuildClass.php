<?php

namespace judahnator\DiscordHttpWrapper\Tests;

use judahnator\DiscordHttpWrapper\Bot;
use judahnator\DiscordHttpWrapper\Channel;
use judahnator\DiscordHttpWrapper\Exceptions\UnauthorizedException;
use judahnator\DiscordHttpWrapper\Guild;
use PHPUnit\Framework\TestCase;

class TestGuildClass extends TestCase
{
    // The ID of the guild we are testing with
    private $GuildID;

    public function setUp()
    {
        require_once dirname(__DIR__).'/vendor/autoload.php';

        // Get the guild ID of the first guild we have access to
        $this->GuildID = (int) Bot::Instance()->guilds[0]->id;

        parent::setUp();
    }

    public function testBasicUsage()
    {

        // Find the guild we will be using
        $Guild = Guild::find($this->GuildID);

        // Sanity check to make sure the id is the id
        $this->assertEquals($this->GuildID, $Guild->id);

        // Make sure channels are being loaded
        $this->assertTrue(is_array($Guild->channels), 'There seems to be problems loading the guilds channels');

        // Make sure the correct class is being setup
        $this->assertEquals(Channel::class, get_class($Guild->channels[0]));
    }

    public function testGuildsNotFound()
    {

        // Setup the exception expected
        $this->expectException(UnauthorizedException::class);
        $this->expectExceptionMessage('You are not authorized to view this guild');

        // Attempt to find an invalid guild
        Guild::find(1);
    }

    public function testSerialization() {

        $Guild = Guild::find($this->GuildID);

        $Serialized = serialize($Guild);

        $this->assertTrue(is_string($Serialized));

        $Unserialized = unserialize($Serialized);

        $this->assertEquals($Guild->name,$Unserialized->name);

    }
}

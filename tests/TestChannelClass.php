<?php

namespace judahnator\DiscordHttpWrapper\Tests;

use judahnator\DiscordHttpWrapper\Bot;
use judahnator\DiscordHttpWrapper\Channel;
use judahnator\DiscordHttpWrapper\Guild;
use judahnator\DiscordHttpWrapper\Message;
use PHPUnit\Framework\TestCase;

class TestChannelClass extends TestCase
{
    /**
     * The channel we will be using.
     *
     * @var Channel
     */
    private $Channel;

    public function setUp()
    {
        require_once dirname(__DIR__).'/vendor/autoload.php';

        // Find the channel we will be using
        $this->Channel = Bot::Instance()->guilds[0]->channels[0];

        parent::setUp();
    }

    public function testBasicUsage()
    {

        // Make sure message retrieval works
        $this->assertTrue(is_array($this->Channel->messages), 'There seems to be an issue reading channel messages');

        // Make sure the guild object is what we expect
        $this->assertEquals(Guild::class, get_class($this->Channel->guild));

        // Make sure the message objects are what we expect
        $this->assertEquals(Message::class, get_class($this->Channel->messages[0]), 'Unexpected object type presented');
    }

    public function testMessageSending()
    {

        // Make sure sending messages works
        $Message = 'Testing sending messages from a channel - '.uniqid();
        $this->assertEquals($Message, $this->Channel->sendMessage($Message)->content);
    }

    public function testBulkMessageDeletion()
    {
        $MessagesToDelete = [];

        for ($i = 0; $i < 5; $i++) {
            $MessagesToDelete[] = (int)$this->Channel->sendMessage('Preparing to test bulk-deleting messages - '.uniqid())->id;
        }

        // Test deleting an individual item
        $this->assertTrue($this->Channel->bulkDeleteMessages([$MessagesToDelete[0]]));

        // Remove the element we removed from the array and re-index
        unset($MessagesToDelete[0]);
        $MessagesToDelete = array_values($MessagesToDelete);

        // Test deleting many items
        $this->assertTrue($this->Channel->bulkDeleteMessages($MessagesToDelete));
    }

    public function testSerialization() {

        $Serialized = serialize($this->Channel);

        $this->assertTrue(is_string($Serialized), 'There was an issue with serialization');

        $this->assertEquals(unserialize($Serialized)->name,$this->Channel->name, 'There was an issue with unserializing the class');

    }
}

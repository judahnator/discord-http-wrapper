<?php

namespace judahnator\DiscordHttpWrapper\Tests;

use judahnator\DiscordHttpWrapper\Bot;
use judahnator\DiscordHttpWrapper\Channel;
use judahnator\DiscordHttpWrapper\Message;
use PHPUnit\Framework\TestCase;

class TestMessageClass extends TestCase
{
    /**
     * @var Channel
     */
    private $Channel;

    public function setUp()
    {

        // Grab the composer autoloader
        require_once dirname(__DIR__).'/vendor/autoload.php';

        // Get the first channel of the first guild we have access to
        $this->Channel = Bot::Instance()->guilds[0]->channels[0];

        parent::setUp();
    }

    public function testFindingMessage()
    {

        // Send a message from the channel and test the ability to find it
        $TestMessage = $this->Channel->sendMessage('Testing finding this message - '.uniqid());

        // Find the message
        $Message = Message::find($this->Channel->id, $TestMessage->id);

        // Sanity check to make sure the id equals the id
        $this->assertEquals($Message->id, $TestMessage->id, 'There seems to be an issue retrieving messages');

        // Make sure we can read a messages content
        $this->assertTrue(is_string($Message->content), 'There seems to be an issue reading the message content');
    }

    public function testSendingMessagesToChannel()
    {

        // Send a message
        $MessageContent = 'Testing sending messages to specific channel - '.uniqid();
        $Message = Message::send((int) $this->Channel->id, $MessageContent);

        // Make sure the message content was successfully transmitted
        $this->assertEquals($MessageContent, $Message->content, 'There seems to be a problem with sending messages');
    }

    public function testReplyingToMessage()
    {

        // Send a message to the channel so we can test replying to it
        $MessageToReplyTo = $this->Channel->sendMessage('I should reply to this message - '.uniqid());

        // Setup message content and reply to previous message
        $MessageContent = 'Replying to a message - '.uniqid();
        $Message = $MessageToReplyTo->reply($MessageContent);

        // Assert the expected value
        $this->assertEquals('<@'.$MessageToReplyTo->author_id.'>	'.$MessageContent, $Message->content, 'Replying to messages seems to have issues');
    }

    public function testDeletingMessage()
    {

        // Get the last message sent in the channel
        $LastMessage = $this->Channel->sendMessage('testing deleting an individual message - '.uniqid());

        // Make sure there are no errors thrown with the delete function
        $this->assertTrue($LastMessage->delete());

        // Make sure the last message ID is changed
        $this->assertNotEquals($LastMessage->id, $this->Channel->getMessages()[0]);
    }

    public function testSerialization() {

        $Message = $this->Channel->messages[0];

        $Serialized = serialize($Message);

        $this->assertTrue(is_string($Serialized));

        $Unserialized = unserialize($Serialized);

        $this->assertEquals($Message->content,$Unserialized->content);

    }
}

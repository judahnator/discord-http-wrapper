<?php

namespace judahnator\DiscordHttpWrapper\Tests;

use judahnator\DiscordHttpWrapper\Bot;
use judahnator\DiscordHttpWrapper\Guild;
use PHPUnit\Framework\TestCase;

class TestBotClass extends TestCase
{
    public function setUp()
    {
        require_once dirname(__DIR__).'/vendor/autoload.php';
        parent::setUp();
    }

    public function testBasicUsage()
    {

        // Asserts that the bot can be successfully loaded
        $this->assertTrue(is_numeric(Bot::Instance()->id), 'The bot may be having authentication issues');

        // Assert that the bot has loaded guilds correctly
        $this->assertTrue(is_array(Bot::Instance()->guilds), 'There seems to be issues loading guilds');

        // Assert that guilds are being loaded correctly
        $this->assertEquals(Guild::class, get_class(Bot::Instance()->guilds[0]));
    }
}
